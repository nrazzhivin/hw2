package ru.mephi.dsbda.hw2_log;

import com.github.benfradet.spark.kafka.writer.KafkaWriter;
import kafka.serializer.StringDecoder;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.SparkConf;
import com.github.benfradet.spark.kafka.writer.DStreamKafkaWriter;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.*;
import scala.Tuple2;
import scala.reflect.ClassTag$;
import scala.runtime.AbstractFunction1;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class TwitterCounter implements Serializable {

    public Map<String, String> kafkaConsumerProperties;
    public Set<String> consumerTopics;
    public Properties kafkaProducerProperties;
    public String producerTopic;

    /**TwitterCounter class constructor
      * @param kafkaConsumerProperties Parameters of Kafka Consumer that receives input data
      * @param consumerTopics Topics from which Kafka Consumer receives input
      * @param kafkaProducerProperties Parameters of Kafka Producer that saves input data
      * @param producerTopic The theme in which Kafka Producer writes the output data*/
    public TwitterCounter(Map<String, String> kafkaConsumerProperties,
                              Set<String> consumerTopics, Properties kafkaProducerProperties,
                              String producerTopic) {
        this.kafkaConsumerProperties = kafkaConsumerProperties;
        this.consumerTopics = consumerTopics;
        this.kafkaProducerProperties = kafkaProducerProperties;
        this.producerTopic = producerTopic;
    }

    /** The main method for calculating the number of messages with different events*/
    public void countPriorities() {
        SparkConf sparkConf = new SparkConf().setAppName("LogPriorityCounter");
        // Creating a context specifying the time interval for generating data packets
        JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(2));
        // Creating direct stream for reading from kafka
        JavaPairInputDStream<String, String> inputDStream = KafkaUtils.createDirectStream(
                jssc,
                String.class,
                String.class,
                StringDecoder.class,
                StringDecoder.class,
                kafkaConsumerProperties,
                consumerTopics);

        JavaPairDStream<String, Iterable<Tuple2<String, Long>>> groupedByHourMessages = countPriorities(inputDStream);
        writeToKafka(groupedByHourMessages.toJavaDStream());
        jssc.start();
        jssc.awaitTermination();
    }

    /**
     / **
      * Calculates the number of messages with different priorities by the clock in the input stream
      * @param inputDStream Input stream
      * @return The result of calculations in the form of {"date / hour": ("event", "number of messages per second")}
     */
    private JavaPairDStream<String, Iterable<Tuple2<String, Long>>> countPriorities(JavaPairInputDStream<String, String> inputDStream) {
        return inputDStream.mapToPair(
                new PairFunction<Tuple2<String, String>, Tuple2<String, String>, Long>() {
                    public Tuple2<Tuple2<String, String>, Long> call(Tuple2<String, String> message) {
                        String key = message._1().split(":", 2)[0];
                        String priority = message._2().split(" ", 2)[0];
                        String value;
                        if (priority.equalsIgnoreCase("create"))
                            value = "CREATE";
                        else if (priority.equalsIgnoreCase("delete"))
                            value = "DELETE";
                        else
                            return null;
                        return new Tuple2<Tuple2<String, String>, Long>(new Tuple2<String, String>(key, "" + value), 1L);
                    }
                }
        ).reduceByKey(new Function2<Long, Long, Long>() {
            public Long call(Long long1, Long long2) throws Exception {
                return long1 + long2;
            }
        }).mapToPair(new PairFunction<Tuple2<Tuple2<String,String>,Long>, String, Tuple2<String,Long>>() {
            public Tuple2<String, Tuple2<String, Long>> call(Tuple2<Tuple2<String, String>, Long> tuple2LongTuple2) throws Exception {
                return new Tuple2<String, Tuple2<String, Long>>(tuple2LongTuple2._1._1, new Tuple2<String, Long>(tuple2LongTuple2._1._2, tuple2LongTuple2._2));
            }
        }).groupByKey();
    }

    /*** Writes data from a given dStream to Kafka
      * @param dStream Output stream */
    private void writeToKafka(JavaDStream<Tuple2<String, Iterable<Tuple2<String, Long>>>> dStream) {

        Tuple2<String, Iterable<Tuple2<String, Long>>> tuple = new Tuple2<String, Iterable<Tuple2<String, Long>>>(null, null);
        KafkaWriter<Tuple2<String, Iterable<Tuple2<String, Long>>>> kafkaWriter =
                new DStreamKafkaWriter<Tuple2<String, Iterable<Tuple2<String, Long>>>>(dStream.dstream(),
                ClassTag$.MODULE$.<Tuple2<String, Iterable<Tuple2<String, Long>>>>apply(tuple.getClass()));

        kafkaWriter.writeToKafka(kafkaProducerProperties,
                new SerializableFunc1<Tuple2<String, Iterable<Tuple2<String, Long>>>, ProducerRecord<String, String>>() {
                    public ProducerRecord<String, String> apply(final Tuple2<String, Iterable<Tuple2<String, Long>>> s) {
                        // Format s: date / time, list of pairs (evemts, number of occurrences)
                        StringBuilder value = new StringBuilder(s._1).append(" :::");
                        for (Tuple2<String, Long> priorityTuple : s._2) {
                            value.append(" ").append(priorityTuple._1).append(":").append(priorityTuple._2).append(";");
                        }
                        return new ProducerRecord<String, String>(producerTopic, value.toString());
                    }
                }
        );
    }

    abstract class SerializableFunc1<T, R> extends AbstractFunction1<T, R> implements Serializable {}

}