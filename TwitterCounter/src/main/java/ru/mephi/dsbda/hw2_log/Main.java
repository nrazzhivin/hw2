package ru.mephi.dsbda.hw2_log;

import java.util.*;

public class Main {

    // Kafka Consumer settings
    private static final Map<String, String> kafkaConsumerProperties = new HashMap<String, String>();
    private static final Set<String> consumerTopics = new HashSet<String>();
    // Kafka Producer settings
    static final Properties kafkaProducerProperties = new Properties();
    final static String producerTopic = "aa"; // Topic

    static {
        consumerTopics.add("pp");

        kafkaConsumerProperties.put("metadata.broker.list", "quickstart.cloudera:9092");
        kafkaConsumerProperties.put("zookeeper.connect", "localhost:2181");
        kafkaConsumerProperties.put("zookeeper.connection.timeout.ms", "1000");

        kafkaProducerProperties.put("bootstrap.servers", "quickstart.cloudera:9092"); // Source list of brokers
        kafkaProducerProperties.put("acks", "all"); // Message acknowledgment mechanism
        kafkaProducerProperties.put("retries", 0); // Number of retries in the absence of confirmation
        kafkaProducerProperties.put("batch.size", 16384); // Message package size
        kafkaProducerProperties.put("linger.ms", 1); // Delay before forming the next message packet
        kafkaProducerProperties.put("buffer.memory", 33554432); // Vendor buffer size
        kafkaProducerProperties.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer"); // Message Key Serializer
        kafkaProducerProperties.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer"); // Message Value Serializer

    }

    public static void main(String[] args) throws Exception {
    	TwitterCounter logPriorityCounter = new TwitterCounter(kafkaConsumerProperties, consumerTopics, kafkaProducerProperties, producerTopic);
        logPriorityCounter.countPriorities();
    }
}