package ru.mephi.dsbda.hw2;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Class that reads the log messages and writes them to Kafka */
public class KafkaLogProducer
{
    Producer<String, String> producer;
    String topic;
    String logFileName;

    /*** KafkaProducer class constructor*/
    public KafkaLogProducer() { }

    /** KafkaProducer class constructor
      * @param props Parameters of Kafka Producer
      * @param topic The subject in which messages will be written
      * @param logFileName The path to the log file*/
    
    
    public KafkaLogProducer(Properties props, String topic, String logFileName) {
        producer = new KafkaProducer<String, String>(props);
        this.topic = topic;
        this.logFileName = logFileName;
    }

    /*** Message generation function for Kafka*/
     
    public void generateMessages() {
        System.out.println("START GEN MESSAGE");
        // General view twitter_stream_1000tweetss.log:
        // date time event message.
        Pattern keyPattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}");
        Matcher matcher;
        try (BufferedReader logReader =
                new BufferedReader(new FileReader(logFileName))) {
            String line;
            int n = 0; //Number of recorded lines
            while ((line = logReader.readLine()) != null) {
                matcher = keyPattern.matcher(line);
                // If the string does not match the format, skip
                if (!matcher.lookingAt())
                    continue;

                // Key - the date and time of the message was recorded
                String key = matcher.group();

                //Value - Event
                String value = line.substring(matcher.end()+1);

                ProducerRecord<String, String> record =
                        new ProducerRecord<String, String>(topic, key, value);
                producer.send(record);
                n++;
            }
            System.out.println("END");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() { this.producer.close();}
}
