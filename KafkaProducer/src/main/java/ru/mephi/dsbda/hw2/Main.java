package ru.mephi.dsbda.hw2;

import java.util.Properties;

public class Main {

    public static void main(String[] args) throws Exception {

        String topic = "pp"; // topic
        String logFileName = "/home/cloudera/Desktop/twitter_stream_1000tweetss.log"; // The path to the log file

        // Object for describing the vendor parameters
        Properties props = new Properties();

        props.put("bootstrap.servers", "quickstart.cloudera:9092"); // Source list of brokers
        props.put("acks", "all"); // Message acknowledgment mechanism
        props.put("retries", 0); // Number of retries in the absence of confirmation
        props.put("batch.size", 16384); // Message package size
        props.put("linger.ms", 1); // Delay before forming the next message packet
        props.put("buffer.memory", 33554432); // Vendor buffer size
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer"); // Message Key Serializer
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer"); // Message Value Serializer

        KafkaLogProducer logProducer = new KafkaLogProducer(props, topic, logFileName);
        logProducer.generateMessages();
        logProducer.close();
    }

}
