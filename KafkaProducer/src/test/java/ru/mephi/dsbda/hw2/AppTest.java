package ru.mephi.dsbda.hw2;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.apache.kafka.clients.producer.MockProducer;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Тестовый класс для KafkaLogProducer
 */
public class AppTest
{
    private MockProducer<String, String> producer;
    private String topic = "test_log";
    private String logFileName = "src/test/testLog";

    /**
     * Предварительная настройка Mock-объекта Kafka Producer'а
     */
    @Before
    public void setUp() {
        producer = new MockProducer<String, String>(
                true, new StringSerializer(), new StringSerializer());
    }

    /**
     * Проверка сообщений, отправляемых в Kafka, на соответствие ожидаемому формату
     * @throws IOException
     */
    @Test
    public void testProducer() throws IOException {
        KafkaLogProducer logProducer = new KafkaLogProducer();
        logProducer.producer = producer;
        logProducer.topic = topic;
        logProducer.logFileName = logFileName;

        logProducer.generateMessages();

        List<ProducerRecord<String, String>> history = producer.history();

        List<ProducerRecord<String, String>> expected = Arrays.asList(
                new ProducerRecord<String, String>(topic, "2017-12-15 20:58:45 create : { id: 941049778305208321, id_str: 941049778305208321, text: RT @beatrooot: English sub for this VLIVE is now up!! \ud83d\ude0a\ud83d\ude0a\ud83d\ude0a"),
                new ProducerRecord<String, String>(topic, "2017-12-15 20:58:47 delete : { id: 941049778305208321, id_str: 941049778305208322, text: RT @beatrooot: English sub for this VLIVE is now up!! \ud83d\ude0a\ud83d\ude0a\ud83d\ude0a"),
                new ProducerRecord<String, String>(topic, "2017-12-15 20:58:45 create : { id: 941049778313539584, id_str: 941049778313539584, text: You need help https://t.co/mEYpOvM5JJ, display_text_range: [0, 13], >Twitter"));


        Assert.assertEquals("Отправленные записи не соответствуют ожидаемым", expected, history);
    }
}
